package com.miusi.astrochallenge

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AstroChallengeApplication : Application()