package com.miusi.astrochallenge.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.miusi.astrochallenge.R

object AppFont {
    val HelveticaFonts = FontFamily(
        Font(R.font.helvetica, FontWeight.Normal),
        Font(R.font.helvetica_light, FontWeight.Light)
    )
}

private val defaultTypography = Typography()
val Typography = Typography(
    displayLarge = defaultTypography.displayLarge.copy(fontFamily = AppFont.HelveticaFonts),
    displayMedium = defaultTypography.displayMedium.copy(fontFamily = AppFont.HelveticaFonts),
    displaySmall = defaultTypography.displaySmall.copy(fontFamily = AppFont.HelveticaFonts),

    headlineLarge = defaultTypography.headlineLarge.copy(fontFamily = AppFont.HelveticaFonts),
    headlineMedium = defaultTypography.headlineMedium.copy(fontFamily = AppFont.HelveticaFonts),
    headlineSmall = defaultTypography.headlineSmall.copy(fontFamily = AppFont.HelveticaFonts),

    titleLarge = defaultTypography.titleLarge.copy(fontFamily = AppFont.HelveticaFonts),
    titleMedium = defaultTypography.titleMedium.copy(fontFamily = AppFont.HelveticaFonts),
    titleSmall = defaultTypography.titleSmall.copy(fontFamily = AppFont.HelveticaFonts),

    bodyLarge = defaultTypography.bodyLarge.copy(fontFamily = AppFont.HelveticaFonts),
    bodyMedium = defaultTypography.bodyMedium.copy(fontFamily = AppFont.HelveticaFonts),
    bodySmall = defaultTypography.bodySmall.copy(fontFamily = AppFont.HelveticaFonts),

    labelLarge = defaultTypography.labelLarge.copy(fontFamily = AppFont.HelveticaFonts),
    labelMedium = defaultTypography.labelMedium.copy(fontFamily = AppFont.HelveticaFonts),
    labelSmall = defaultTypography.labelSmall.copy(fontFamily = AppFont.HelveticaFonts)
)
