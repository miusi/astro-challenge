package com.miusi.astrochallenge.ui.theme

enum class Orientation {

    Portrait, Landscape

}