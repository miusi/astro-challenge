package com.miusi.astrochallenge.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val lightBlue = Color(0xFF8EA4D2)
val blue = Color(0xFF6279b8)
val darkBlue = Color(0xFF49516F)
val darkGreen = Color(0xFF496F5D)
val green = Color(0xFF4C9F70)