package com.miusi.astrochallenge.di

import com.miusi.astrochallenge.data.dataSource.remote.repository.WeatherRepositoryImpl
import com.miusi.astrochallenge.data.dataSource.remote.service.WeatherService
import com.miusi.astrochallenge.domain.repository.WeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideWeatherRepository(weatherService: WeatherService): WeatherRepository =
        WeatherRepositoryImpl(weatherService)

}