@file:OptIn(ExperimentalMaterial3Api::class)

package com.miusi.astrochallenge

import DefaultNavigationDrawer
import android.Manifest
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.miusi.astrochallenge.presentation.utils.RequestPermission
import com.miusi.astrochallenge.ui.theme.AppTheme
import com.miusi.astrochallenge.ui.theme.AstroChallengeTheme
import com.miusi.astrochallenge.ui.theme.rememberWindowSizeClass
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class, ExperimentalPermissionsApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContent {
            val window = rememberWindowSizeClass()
            AstroChallengeTheme(window) {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    RequestPermission(permission = Manifest.permission.ACCESS_FINE_LOCATION)
                    DefaultNavigationDrawer()
                }
            }
        }
    }
}





