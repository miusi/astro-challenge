package com.miusi.astrochallenge.core.util

data class ErrorResponse(
    val statusCode: Int = 500,
    val message: String = ""
)