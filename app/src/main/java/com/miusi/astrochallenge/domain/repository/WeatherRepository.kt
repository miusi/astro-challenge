package com.miusi.astrochallenge.domain.repository

import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.domain.model.CurrentWeather
import kotlinx.coroutines.flow.Flow

interface WeatherRepository {
    suspend fun getWeatherInfo(lat: Double, lon: Double, units: String): Flow<Resource<CurrentWeather>>
}