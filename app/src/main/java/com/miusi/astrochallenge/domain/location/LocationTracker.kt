package com.miusi.astrochallenge.domain.location

import android.location.Location

interface LocationTracker {
    suspend fun trackLocation(): Location?
}