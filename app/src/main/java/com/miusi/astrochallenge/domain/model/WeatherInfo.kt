package com.miusi.astrochallenge.domain.model

data class WeatherInfo(
    val currentWeather: CurrentWeather,
    val dailys: List<Daily>,
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val timezone_offset: Int
)