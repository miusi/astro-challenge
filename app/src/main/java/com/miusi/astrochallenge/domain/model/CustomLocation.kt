package com.miusi.astrochallenge.domain.model

data class CustomLocation(val lat: Double, val lon: Double)