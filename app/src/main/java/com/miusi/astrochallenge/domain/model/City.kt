package com.miusi.astrochallenge.domain.model

data class City(
    val id: String,
    val name: String,
    val country: String,
    val customLocation: CustomLocation
) {
    companion object {
        val listOfCities = listOf(
            City(
                id = "1",
                name = "Buenos Aires",
                country = "Argentina",
                CustomLocation(-34.60196110305195, -58.39611996960769)
            ),
            City(
                id = "2",
                name = "Munich",
                country = "Alemania",
                CustomLocation(48.13770063573377, 11.579942177406803)
            ),
            City(
                id = "3",
                name = "San Pablo",
                country = "Brasil",
                CustomLocation(-23.55503649482655, -46.64206462737786)
            ),
            City(
                id = "4",
                name = "Londres",
                country = "Inglaterra",
                CustomLocation(51.509299079243085, -0.11671365936302813)
            ),
            City(
                id = "5",
                name = "Montevideo",
                country = "Uruguay",
                CustomLocation(-34.90609404672333, -56.18323356637458)
            ),
        )
    }
}




