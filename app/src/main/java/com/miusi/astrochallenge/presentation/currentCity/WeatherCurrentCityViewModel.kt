package com.miusi.astrochallenge.presentation.currentCity

import android.location.Location
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.domain.location.LocationTracker
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.domain.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherCurrentCityViewModel @Inject constructor(
    private val locationTracker: LocationTracker,
    private val weatherRepository: WeatherRepository,
) : ViewModel() {
    var currentLocation by mutableStateOf<Location?>(null)
    var weatherResponse by mutableStateOf<Resource<CurrentWeather>?>(null)
        private set

    init {
        getCurrentLocation()
    }

    fun getCurrentLocation() {
        viewModelScope.launch {
            locationTracker.trackLocation().let {
                currentLocation = it
                getCurrentLocationWeather()
            }
        }
    }

    fun getCurrentLocationWeather() {
        viewModelScope.launch {
            getCurrentLocation()
            if (currentLocation != null) {
                weatherRepository.getWeatherInfo(
                    currentLocation!!.latitude,
                    currentLocation!!.longitude,
                    "METRIC"
                ).collect {
                    weatherResponse = it
                }
            }
        }
    }
}