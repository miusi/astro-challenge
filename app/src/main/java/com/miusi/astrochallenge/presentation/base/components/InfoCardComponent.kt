package com.miusi.astrochallenge.presentation.base

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
internal fun WeatherInfoCard(cardTitle: String, value: String) {
        Card(
            colors = CardDefaults.cardColors(containerColor = Color.LightGray.copy(alpha = 0.5f)),
            modifier = Modifier
                .height(80.dp)
                .width(80.dp)
                .fillMaxSize(),
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center
            ) {
                Spacer(modifier = Modifier.weight(0.5f))
                Text(text = cardTitle, fontFamily = FontFamily.Serif, textAlign = TextAlign.Center)
                Spacer(modifier = Modifier.weight(0.5f))
                Text(
                    text = value,
                    fontFamily = FontFamily.Serif,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,

                    textAlign = TextAlign.Center
                )
                Spacer(modifier = Modifier.weight(1f))
            }
        }

    }

@Preview
@Composable
fun PreviewWeatherInfoCard(){
    WeatherInfoCard(cardTitle = "Visibility", value = "10000 m")
}