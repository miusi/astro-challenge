package com.miusi.astrochallenge.presentation

sealed class Screens(val route: String){
    object WeatherSelectedCity: Screens("city/{id}"){
        fun passCityId(id: String = "1") = "city/$id"
    }

    object WeatherCurrentCity: Screens("city/current")
    object WeatherLanding: Screens("landing")
}
