package com.miusi.astrochallenge.presentation.selectedCity

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.presentation.components.ProgressBar

@Composable
fun WeatherSelectedCityScreen(
    viewModel: WeatherSelectedCityViewModel = hiltViewModel(),
    navController: NavHostController,
    idParam: String
) {
    when (val response = viewModel.weatherResponse) {
        is Resource.Loading -> {
            ProgressBar()
        }

        is Resource.Failure -> {
            Toast.makeText(
                LocalContext.current,
                response.message,
                Toast.LENGTH_SHORT
            ).show()
        }

        is Resource.Success -> {
            WeatherSelectedCityContent(
                currentWeather = response.data,
            )
        }

        else -> {
            ProgressBar()
        }
    }
}




