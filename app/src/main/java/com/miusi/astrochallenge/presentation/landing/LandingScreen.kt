import android.Manifest
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.miusi.astrochallenge.presentation.utils.RequestPermission
import com.miusi.astrochallenge.ui.theme.AppTheme
import com.miusi.astrochallenge.ui.theme.Orientation

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun LandingScreen(
    navController: NavHostController
) {
    val openDialog = remember { mutableStateOf(false) }
    if (openDialog.value) {
        RequestPermission(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    }

    Column(
        Modifier
            .fillMaxSize()
            .padding(horizontal = 50.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center

    ) {
        Text(
            text = "We need your location to enable some features. \n You can pick any city from the menu too!",
            textAlign = TextAlign.Center,
            fontSize = 20.sp
        )
        Spacer(modifier = Modifier.height(100.dp))
        Button(onClick = { openDialog.value = true }) {
            Text(text = "Enable location")
        }
    }

}

@Preview(heightDp = 360, widthDp = 800)
//@Preview
@Composable
fun PreviewLandingScreen() {
    LandingScreen(navController = rememberNavController())
}