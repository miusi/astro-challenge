package com.miusi.astrochallenge.presentation.selectedCity

import android.location.Location
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.domain.model.City
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.domain.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherSelectedCityViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private var currentSelectedCity by mutableStateOf(City.listOfCities[0])
    private val navigationArgs = savedStateHandle.get<String>("id")

    var weatherResponse by mutableStateOf<Resource<CurrentWeather>?>(null)
        private set

    init {
        getSelectedCityWeather()
    }

    private fun getSelectedCityWeather() {
        weatherResponse = Resource.Loading
        checkForCityId()
        viewModelScope.launch {
            weatherRepository.getWeatherInfo(
                currentSelectedCity.customLocation.lat,
                currentSelectedCity.customLocation.lon,
                "METRIC"
            ).collect {
                weatherResponse = it
            }
        }
    }

    // Needs refactor
    private fun checkForCityId() {
        when (navigationArgs) {
            "1" -> {
                currentSelectedCity = City.listOfCities[0] // Buenos Aires
            }

            "2" -> {
                currentSelectedCity = City.listOfCities[1] // Munich
            }

            "3" -> {
                currentSelectedCity = City.listOfCities[2] // San Pablo
            }

            "4" -> {
                currentSelectedCity = City.listOfCities[3] // Londres
            }

            "5" -> {
                currentSelectedCity = City.listOfCities[4] // Montevideo
            }

            else -> {
                currentSelectedCity = City.listOfCities[0] // Buenos Aires
            }
        }
    }
}