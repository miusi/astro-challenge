package com.miusi.astrochallenge.presentation.currentCity

import androidx.compose.runtime.Composable
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.presentation.base.WeatherInformation

@Composable
fun WeatherCurrentCityContent(
    currentWeather: CurrentWeather
) {
    WeatherInformation(currentWeather)
}