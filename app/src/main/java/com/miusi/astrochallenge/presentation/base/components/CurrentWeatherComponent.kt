package com.miusi.astrochallenge.presentation.base

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.miusi.astrochallenge.domain.model.Clouds
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.domain.model.Main
import com.miusi.astrochallenge.domain.model.Weather
import com.miusi.astrochallenge.domain.model.Wind
import com.miusi.astrochallenge.ui.theme.AppTheme
import com.miusi.astrochallenge.ui.theme.Orientation
import kotlin.math.roundToInt

@Composable
fun WeatherInformation(currentWeather: CurrentWeather) {
    if (AppTheme.orientation == Orientation.Portrait) {
        PortraitInfo(currentWeather)
    } else {
        LandscapeInfo(currentWeather)
    }
}

@Composable
private fun PortraitInfo(currentWeather: CurrentWeather) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            Row(modifier = Modifier.fillMaxWidth()) {
                Icon(imageVector = Icons.Default.LocationOn, contentDescription = "Location")
                Text(text = currentWeather.name, fontFamily = FontFamily.Serif)
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "${currentWeather.main.temp.roundToInt()}°",
                    fontSize = 100.sp,
                    fontWeight = FontWeight.Bold
                )
                AsyncImage(
                    modifier = Modifier
                        .size(200.dp),
                    model = "http://openweathermap.org/img/w/${currentWeather.weather[0].icon}.png",
                    contentDescription = "Weather icon"
                )
            }
            Text(text = currentWeather.weather.first().main, fontSize = 50.sp)
            Row() {
                Text(text = "H:${currentWeather.main.temp_max}°", fontFamily = FontFamily.Serif)
                Spacer(modifier = Modifier.width(10.dp))
                Text(text = "L:${currentWeather.main.temp_min}°", fontFamily = FontFamily.Serif)
            }
            Spacer(modifier = Modifier.height(100.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                WeatherInfoCard(cardTitle = "Humidity", value = "${currentWeather.main.humidity}%")
                WeatherInfoCard(cardTitle = "Wind", value = "${currentWeather.wind.speed} km/h")
                WeatherInfoCard(
                    cardTitle = "Visibility",
                    value = "${currentWeather.visibility} mts"
                )
            }
        }
    }
}

@Composable
private fun LandscapeInfo(currentWeather: CurrentWeather) {
    Row(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp)
    ) {
        Row {
            Spacer(modifier = Modifier.weight(1f))
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top
            ) {
                Row {
                    Icon(imageVector = Icons.Default.LocationOn, contentDescription = "Location")
                    Text(text = currentWeather.name, fontFamily = FontFamily.Serif)
                }
                Row() {
                    Text(
                        text = "${currentWeather.main.temp.roundToInt()}°",
                        fontSize = 100.sp,
                        fontWeight = FontWeight.Bold
                    )
                    AsyncImage(
                        modifier = Modifier
                            .size(200.dp),
                        model = "http://openweathermap.org/img/w/${currentWeather.weather[0].icon}.png",
                        contentDescription = "Weather icon"
                    )
                }
                Text(text = currentWeather.weather.first().main, fontSize = 50.sp)
                Row(horizontalArrangement = Arrangement.SpaceBetween) {
                    Text(text = "H:${currentWeather.main.temp_min}°", fontFamily = FontFamily.Serif)
                    Spacer(modifier = Modifier.height(50.dp))
                    Text(text = "L:${currentWeather.main.temp_min}°", fontFamily = FontFamily.Serif)
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Column(
                modifier = Modifier.fillMaxHeight(),
                verticalArrangement = Arrangement.SpaceAround
            ) {
                Spacer(modifier = Modifier.height(20.dp))
                WeatherInfoCard(cardTitle = "Humidity", value = "${currentWeather.main.humidity}%")
                WeatherInfoCard(cardTitle = "Wind", value = "${currentWeather.wind.speed} km/h")
                WeatherInfoCard(
                    cardTitle = "Visibility",
                    value = "${currentWeather.visibility} mts"
                )
                Spacer(modifier = Modifier.height(20.dp))
            }
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}

@Preview(heightDp = 360, widthDp = 800) // LandScape
//@Preview // Portrait
@Composable
fun PreviewWeatherContent() {
    LandscapeInfo(
        currentWeather =
        CurrentWeather(
            Clouds(100),
            main = Main(
                temp = 10.0,
                feels_like = 11.5,
                humidity = 98,
                pressure = 5,
                temp_max = 15.5,
                temp_min = 8.9
            ),
            Wind(17.8, 5, 80.0),
            "Prueba",
            10.0,
            15,
            50.4,
            15,
            20,
            8,
            19,
            10.0,
            15.2,
            400,
            listOf(Weather("clear sky", "01n", 800, "Clear")),
            90,
            45.0
        )
    )
}