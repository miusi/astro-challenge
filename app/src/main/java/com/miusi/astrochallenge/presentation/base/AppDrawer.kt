@file:JvmName("DefaultNavigationDrawerKt")

package com.miusi.astrochallenge.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavHostController
import com.miusi.astrochallenge.R
import com.miusi.astrochallenge.domain.model.City
import com.miusi.astrochallenge.presentation.Screens

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppDrawer(
    route: String,
    modifier: Modifier = Modifier,
    closeDrawer: () -> Unit = {},
    navController: NavHostController
) {
    ModalDrawerSheet(modifier = Modifier) {
        DrawerHeader(modifier)
        Spacer(modifier = Modifier.padding(dimensionResource(id = R.dimen.spacer_padding)))
        LazyColumn {
            items(City.listOfCities){ item ->
                NavigationDrawerItem(
                    label = {
                        Text(
                            text = item.name,
                            style = MaterialTheme.typography.bodyMedium
                        )
                    },
                    selected = route == Screens.WeatherSelectedCity.route,
                    onClick = {
                        navController.navigate(route = Screens.WeatherSelectedCity.passCityId(item.id))
                        closeDrawer()
                    },
                    shape = MaterialTheme.shapes.small
                )
            }
        }
        Column() {
            NavigationDrawerItem(
                label = {
                    Text(
                        text = "Your city",
                        style = MaterialTheme.typography.bodyMedium
                    )
                },
                selected = route == Screens.WeatherCurrentCity.route,
                onClick = {
                    navController.navigate(route = Screens.WeatherCurrentCity.route)
                    closeDrawer()
                },
                shape = MaterialTheme.shapes.small
            )
        }
    }
}


@Composable
fun DrawerHeader(modifier: Modifier) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
        modifier = modifier
            .background(MaterialTheme.colorScheme.secondary)
            .padding(dimensionResource(id = R.dimen.header_padding))
            .fillMaxWidth()
    ) {
        Text(
            text = stringResource(id = R.string.app_name),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.bodyLarge,
            color = MaterialTheme.colorScheme.onPrimary,
        )
    }
}
