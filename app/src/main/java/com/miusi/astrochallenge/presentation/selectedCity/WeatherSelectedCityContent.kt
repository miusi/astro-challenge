package com.miusi.astrochallenge.presentation.selectedCity

import androidx.compose.runtime.Composable
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.presentation.base.WeatherInformation

@Composable
fun WeatherSelectedCityContent(
    currentWeather: CurrentWeather
) {
    WeatherInformation(currentWeather = currentWeather)
}