package com.miusi.astrochallenge.presentation.currentCity

import android.Manifest
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.presentation.components.ProgressBar
import com.miusi.astrochallenge.presentation.utils.RequestPermission

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun WeatherCurrentCityScreen(
    viewModel: WeatherCurrentCityViewModel = hiltViewModel(),
    navController: NavHostController,
) {
    when (val response = viewModel.weatherResponse) {
        is Resource.Loading -> {
            ProgressBar()
        }

        is Resource.Failure -> {
            LocationNotFoundContent()
        }

        is Resource.Success -> {
            WeatherCurrentCityContent(
                currentWeather = response.data,
            )
        }

        else -> { }
    }
}

@Composable
@OptIn(ExperimentalPermissionsApi::class)
private fun LocationNotFoundContent() {
    Column(
        modifier = Modifier.fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = "We couldn't reach your location")
        RequestPermission(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    }
}

@Preview
@Composable
private fun PreviewLocationNotFound(){
    LocationNotFoundContent()
}


