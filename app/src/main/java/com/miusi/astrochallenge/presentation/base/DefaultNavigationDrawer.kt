import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.miusi.astrochallenge.presentation.components.AppDrawer
import com.miusi.astrochallenge.presentation.Screens
import com.miusi.astrochallenge.presentation.currentCity.WeatherCurrentCityScreen
import com.miusi.astrochallenge.presentation.selectedCity.WeatherSelectedCityScreen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DefaultNavigationDrawer(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
) {
    val currentNavBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute =
        currentNavBackStackEntry?.destination?.route ?: Screens.WeatherSelectedCity.passCityId("1")

    ModalNavigationDrawer(drawerContent = {
        AppDrawer(
            route = currentRoute,
            closeDrawer = { coroutineScope.launch { drawerState.close() } },
            modifier = Modifier,
            navController = navController
        )
    }, drawerState = drawerState) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(text = "") },
                    modifier = Modifier.fillMaxWidth(),
                    navigationIcon = {
                        IconButton(onClick = {
                            coroutineScope.launch { drawerState.open() }
                        }, content = {
                            Icon(
                                imageVector = Icons.Default.Menu, contentDescription = null
                            )
                        })
                    },
                    colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.Transparent)
                )
            }, modifier = Modifier
        ) {
            NavHost(
                navController = navController,
                startDestination = Screens.WeatherLanding.route,
                modifier = modifier.padding(it)
            ) {

                composable(
                    route = Screens.WeatherSelectedCity.route,
                    arguments = listOf(navArgument("id") {
                        type = NavType.StringType
                    })
                ) {
                    it.arguments?.getString("id")?.let { cityId ->
                        WeatherSelectedCityScreen(navController = navController, idParam = cityId)
                    }
                }

                composable(
                    route = Screens.WeatherCurrentCity.route
                ) {
                    WeatherCurrentCityScreen(navController = navController)
                }

                composable(Screens.WeatherLanding.route) {
                    LandingScreen(navController = navController)
                }
            }
        }
    }
}