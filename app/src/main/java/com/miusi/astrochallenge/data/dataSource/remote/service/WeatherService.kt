package com.miusi.astrochallenge.data.dataSource.remote.service

import com.miusi.astrochallenge.core.Config
import com.miusi.astrochallenge.domain.model.CurrentWeather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("weather")
    suspend fun getCurrent(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") units: String,
        @Query("appid") appid: String = Config.API_KEY
    ) : Response<CurrentWeather>

}