package com.miusi.astrochallenge.data.dataSource.remote.dto

import com.miusi.astrochallenge.domain.model.Weather

data class WeatherDto(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String,
) {
    fun toWeather(): Weather {
        return Weather(
            description = description,
            icon = icon,
            id = id,
            main = main
        )
    }
}