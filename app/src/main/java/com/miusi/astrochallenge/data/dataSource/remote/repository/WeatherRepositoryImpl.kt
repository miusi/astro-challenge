package com.miusi.astrochallenge.data.dataSource.remote.repository

import com.miusi.astrochallenge.core.util.Resource
import com.miusi.astrochallenge.core.util.ResponseToRequest
import com.miusi.astrochallenge.data.dataSource.remote.service.WeatherService
import com.miusi.astrochallenge.domain.model.CurrentWeather
import com.miusi.astrochallenge.domain.repository.WeatherRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class WeatherRepositoryImpl(private val weatherService: WeatherService) : WeatherRepository {
    override suspend fun getWeatherInfo(
        lat: Double,
        lon: Double,
        units: String
    ): Flow<Resource<CurrentWeather>> = flow {
        try {
            ResponseToRequest.send(weatherService.getCurrent(lat, lon, units)).run {
                when(this) {
                    is Resource.Success -> {
                        emit(Resource.Success(this.data))
                    }
                    else -> {
                        emit(Resource.Failure("Error desconocido"))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Resource.Failure("Hubo una excepción: ${e.message}"))
        }
    }
}